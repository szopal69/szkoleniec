﻿using Obiekty;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KlasyPomocnicze
{
    public static class RodzinaExt
    {
        public static List<Dziecko> zwrocListeDzieci(this Rodzina rodzina)
        {
            var dzieci = rodzina.ListaUzytkownikow.Where(p => p is Dziecko);
            var wzrost = dzieci.Select(p => (Dziecko)p);
            return wzrost.ToList();
        }
    }
}
