﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace KlasyPomocnicze
{
    public static class BinarySerializator
    {
        private static object lockObject = new object();
        public static string Serializuj(object objekt, string sciezka = null)
        {
            string nazwaKlasy = objekt.GetType().ToString();
            nazwaKlasy = nazwaKlasy.Substring(nazwaKlasy.LastIndexOf(".") + 1);

            sciezka = sciezka ?? nazwaKlasy + DateTime.Now.ToString("yyyyMMddHHmmss") + ".dat";
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            try
            {
                lock (lockObject)
                { 
                    using (FileStream fileStream = new FileStream(sciezka, FileMode.CreateNew))
                    {
                        binaryFormatter.Serialize(fileStream, objekt);
                    }
                }
            }
            catch (IOException io)
            {
                Console.WriteLine("Nie mozna utworzyc pliku");
            }
            catch (SerializationException se)
            {
                Console.WriteLine("Klasa " + nazwaKlasy + " nie jest serializowana!");
            }
            catch (Exception e)
            {
                Console.WriteLine("Wystapil wyjatek podczas uruchamiania programu");
                Console.WriteLine(e.Message);
            }

            return sciezka;
        }

        public static T Deserializuj <T> (string nazwaPliku)
        {
            
            BinaryFormatter binaryFormatter = new BinaryFormatter();

            try
            {
                lock (lockObject)
                {
                    using (FileStream fileStream = new FileStream(nazwaPliku, FileMode.Open))
                    {
                        return (T)binaryFormatter.Deserialize(fileStream);
                    }
                }
            }
            catch (IOException io)
            {
                Console.WriteLine("Nie mozna utworzyc pliku");
            }
            catch (SerializationException se)
            {
                Console.WriteLine("Plik " + nazwaPliku + " nie jest serializowany!");
            }
            catch (Exception e)
            {
                Console.WriteLine("Wystapil wyjatek podczas uruchamiania programu");
                Console.WriteLine(e.Message);
            }

            return default(T);
        }

    }
}
