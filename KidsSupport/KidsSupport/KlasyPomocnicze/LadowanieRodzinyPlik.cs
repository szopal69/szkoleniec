﻿using Obiekty;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace KlasyPomocnicze
{
    public class LadowanieRodzinyPlik
    {
        public Rodzina LadujRodzina(string sciezka)
        {
            using (StreamReader streamReader = new StreamReader(sciezka))
            {
                string linia;
                var userList = new List<Uzytkownik>();
                Rodzina rodzina = new Rodzina(userList);

                while ((linia = streamReader.ReadLine()) != null)
                {
                    Regex reg = new Regex(
                        "([a-zA-Zęóąśłżźćń]+);([a-zA-Zęóąśłżźćń]+);([\\d]*);([\\d]*);([\\d]*);(([\\w]{2}\\.)\\s([a-zA-Zęóąśłżźćń]+)\\s([\\w]+)\\/?([\\w]*)\\s+([\\d{2}\\-\\d{3}]+)\\s([a-zA-Zęóąśłżźćń\\-]+))?;");
                    var result = reg.Match(linia);

                    var imie = result.Groups[1].Value;
                    var nazwisko = result.Groups[2].Value;
                    var wiek = result.Groups[3].Value;
                    var wzrost = result.Groups[4].Value;
                    var waga = result.Groups[5].Value;
                    var adres = result.Groups[6].Value;
                    var prefiks = result.Groups[7].Value;
                    var ulica = result.Groups[8].Value;
                    var numerDomu = result.Groups[9].Value;
                    var numerMieszkania = result.Groups[10].Value;
                    var kodPocztowy = result.Groups[11].Value;
                    var miejscowoscPoczty = result.Groups[12].Value;
                    Console.WriteLine("\n-------------> Odczytana linia w pliku:\n" + linia);
                    if (result.Success)
                    {
                        Console.WriteLine("=========\nImie:" + imie + 
                                          "\nNazwisko:" + nazwisko + 
                                          "\nWiek:" + wiek + 
                                          "\nWzrost:" + wzrost + 
                                          "\nWaga:" + waga + 
                                          "\nAdres:" + prefiks + " " + ulica + " " + numerDomu + " " + numerMieszkania + " " + kodPocztowy + " " + miejscowoscPoczty);
                        if (waga.Length > 0 && wzrost.Length > 0 && wiek.Length > 0)
                        {
                            if (adres.Length > 0)
                            {
                                rodzina.ListaUzytkownikow.Add(new Dziecko(imie, nazwisko, Int32.Parse(waga),
                                    Int32.Parse(wzrost), Int32.Parse(wiek),new Adres(adres)));
                            }
                            else
                            {
                                rodzina.ListaUzytkownikow.Add(new Dziecko(imie, nazwisko, Int32.Parse(waga),
                                    Int32.Parse(wzrost), Int32.Parse(wiek)));
                            }
                        }
                        else
                        {
                            if (adres.Length > 0)
                            {
                                rodzina.ListaUzytkownikow.Add(new Rodzic(imie, nazwisko, adresUzytkownika: new Adres(adres)));
                            }
                            else
                            {
                                rodzina.ListaUzytkownikow.Add(new Rodzic(imie, nazwisko));
                            }
                        }
                    }
                }

                return rodzina;
            }

            return null;
        }
    }
}
