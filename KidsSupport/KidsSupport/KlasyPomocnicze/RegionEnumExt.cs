﻿using System;
using System.Collections.Generic;
using System.Text;
using Obiekty;

namespace KlasyPomocnicze
{
    public static class RegionEnumExt
    {
        public static bool CzyPolskaPoludniowa(this KodPocztowy.RegionEnum regionEnum)
        {
            switch (regionEnum)
            {
                case KodPocztowy.RegionEnum.Krakowski:
                    Console.WriteLine("Jestem w Krakowie");
                    goto case KodPocztowy.RegionEnum.Katowicki;
                case KodPocztowy.RegionEnum.Katowicki:
                    return true;
                default:
                    return false;
            }
            return false;
        }
    }
}
