﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace KidsSupport.WygenerowanyZBazy
{
    public partial class SzkolenieLSzopaContext : DbContext
    {
        public SzkolenieLSzopaContext()
        {
        }

        public SzkolenieLSzopaContext(DbContextOptions<SzkolenieLSzopaContext> options)
            : base(options)
        {
        }

        public virtual DbSet<TabelaTestowa> TabelaTestowa { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=APS00079033-MB\\SQLEXPRESS;Database=SzkolenieLSzopa;Integrated Security=SSPI;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.6-servicing-10079");

            modelBuilder.Entity<TabelaTestowa>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Nazwa).HasMaxLength(100);
            });
        }
    }
}
