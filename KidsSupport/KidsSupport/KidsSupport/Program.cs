﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Data.Odbc;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;
using KidsSupport.WygenerowanyZBazy;
using KlasyPomocnicze;
using Microsoft.EntityFrameworkCore;
using Obiekty;
using Obiekty.Database;

//=========================================================================
//    Poczatkowe informacje z C#
//
//int pierwszaLiczba = 10;
//double pierwszaLiczbaRzeczywista = 10.3;
/* char pierwszyZnak = 'A';
 string pierwszyString = "Ala ma kota";
 bool prawda = true;
 decimal kasa = 10.50M;
 print(pierwszaLiczba + pierwszaLiczbaRzeczywista + pierwszyZnak + pierwszyString + prawda);
 print("Hello World!");*/

/*long zmiennaLong = pierwszaLiczba; implicit conversion - rzutowanie niejawne
pierwszaLiczba = (int) zmiennaLong; explicit conversion - rzutowanie ze strata danych*/

//DodajJeden(out pierwszaLiczba);
//==========================================================================


namespace KidsSupport
{
    class Program
    {
        //===========================================================================================
        //        STALE POLA W PROGRAMIE 
        //====                                                                                   ====

        //sciezka dostepu do pliku ktory zostal wygenerowany w opcji GenerujDane
        private static string pathGenerujDane = "C:\\Szkolenie_NET\\C_sharp_GIT\\KidsSupport\\KidsSupport\\KidsSupport\\bin\\GenerujDane\\netcoreapp2.2\\";

        //parametry polaczenia do MS SQL
        private static string connectionString =
            @"Server=APS00079033-MB\SQLEXPRESS;Database=SzkolenieLSzopa;Integrated Security=SSPI;";

        //parametry polaczenia do MS SQL przez ODBC
        private static string odbcStringConnection = "Dsn=SzkolenieODBC";
        //===========================================================================================


        static void Main(string[] args)
        {
            Dziecko dzieckoNowe = new Dziecko("Jan", "Kowalski", 20, 123, 12);
            print(dzieckoNowe.PrzedstawSie());

            Osoba osobaNowa = new Dziecko("MAdzia", "Kowal", 3, 6, 8);
            print(osobaNowa.PrzedstawSie());

            #region SLOWNIKI
            Dictionary<string, string> slownik = new Dictionary<string, string>();
            slownik.Add("klucz1", "wartosc1");
            //slownik.Add("klucz2", "wartosc2");
            slownik.Add("klucz3", "wartosc3");
           // slownik.TryAdd("klucz2", "wartosc4");
            slownik["klucz2"] = "wartosc5";
            foreach (var el in slownik)
            {
                print("Klucz:" + el.Key.ToString() + " wartosc:" + el.Value.ToString());
            }

            List<KeyValuePair<string, string>> listaPar = new List<KeyValuePair<string, string>>();
            listaPar.Add(new KeyValuePair<string, string>("klucz1", "wartosc1"));
            listaPar.Add(new KeyValuePair<string, string>("klucz2", "wartosc2"));
            listaPar.Add(new KeyValuePair<string, string>("klucz1", "wartosc3"));

            foreach (var el in listaPar)
            {
                print("Klucz:" + el.Key.ToString() + " wartosc:" + el.Value.ToString());
            }

            List<Dictionary<string, string>> listaSlownikow = new List<Dictionary<string, string>>();
            #endregion

            #region DYNAMICZNE_ZMIENNE
            dynamic x = 2;
            print("Test dynamic:" + x);
            x = "slowo jakies";
            print("Test dynamic2:" + x);
            #endregion

            KodPocztowy.RegionEnum region = KodPocztowy.RegionEnum.Olsztynski;

            print(RegionEnumExt.CzyPolskaPoludniowa(region).ToString());


            #region TABLICE_LISTY
            int[,] tablica = {{1, 2, 3}, {4, 5, 6}};
            int[][] tablicaTab = new int[2][];

            tablicaTab[0] = new int[] { 1, 2, 3};
            tablicaTab[1] = new int[] {4, 5, 6, 7, 8};

            List<int> kolekcja = tablicaTab[1].Select(p =>
            {
                print("Wypis z selecta:" + p.ToString() + " aktualny czas:" + DateTime.UtcNow);
                return p;
            }).ToList();

            foreach (int elem in kolekcja)
            {
                print(elem.ToString());
                Thread.Sleep(1000);
            }

            foreach (int elem in kolekcja)
            {
                print(elem.ToString());
                Thread.Sleep(1000);
            }

            foreach (int el in tablica)
            {
                print(el.ToString());
                Thread.Sleep(1000);
            }



            for (int i = 0; i < tablica.GetLength(0); i++)
            {
                for (int j = 0; j < tablica.GetLength(1); j++)
                {
                    print(tablica[i, j].ToString());
                }
            }

            for (int i = 0; i < tablicaTab.Length; i++)
            {
                for(int j=0; j < tablicaTab[i].Length; j++)
                {
                    print(tablicaTab[i][j].ToString());
                }
            }

            #endregion

            var parzyste = ZwrocParzyste(kolekcja);

            System.Environment.Exit(1);

            #region PRZECIAZENIA_OPERATOROW
            Uzytkownik mama = new Uzytkownik("Janina", "Nowacka", 34, new Adres("ul. Żonkilowa 34/2 38-124 Wiśniowa"));
            Uzytkownik tata = new Uzytkownik("Mariusz", "Nowacki", 36, new Adres("ul. Żonkilowa 34/2 38-124 Wiśniowa"));
            Rodzina rodzinaKowalskich = mama + tata;
            Uzytkownik dziecko = new Uzytkownik("Piotr", "Nowacki", 10, new Adres("ul. Żonkilowa 34/2 38-124 Wiśniowa"));
            rodzinaKowalskich += dziecko;
            #endregion

#if GENERUJDANE
            #region GENEROWANIE_DANYCH_DO_PLIKU
            DodajDaneInit();
            #endregion

#else
            #region ODCZYTAJ_ADRES_RODZINA_DZIECI_Z_PLIKU


            Adres adresPoDeserializacji = BinarySerializator.Deserializuj<Adres>(pathGenerujDane + "Adres20190829090405.dat");
            print("Adres po deserializacji:\n" + adresPoDeserializacji.ToString());

            Rodzina rodzinaPoDeserializacji = BinarySerializator.Deserializuj<Rodzina>(pathGenerujDane + "Rodzina20190829090405.dat");
            print("Rodzina po deserializacji:\n" + rodzinaPoDeserializacji.ToString());

            //rodzinaPoDeserializacji.ListaUzytkownikow.Where(p => p.GetType().Equals(typeof (Dziecko)));
            rodzinaPoDeserializacji.zwrocListeDzieci().ForEach(p => print(p.ToString()));
#endregion

#region ODCZYTAJ_DANE_Z_PLIKU_PLASKIEGO_POPRZEZ_REGEX
            LadowanieRodzinyPlik ladowanieRodziny = new LadowanieRodzinyPlik();
            var rodzina = ladowanieRodziny.LadujRodzina("dane.txt");
#endregion


#region ODCZYT_Z_BAZY_DANYCH
            //====================================================================================================
            //       SQLConnection
            //
            print("=================================== ODCZYTYWANIE I ZAPIS DO BAZY DANYCH ====================");
            using (SqlConnection sqlConnection =
                new SqlConnection(connectionString))
            {

                sqlConnection.Open();

                string query = "SELECT * FROM dbo.TabelaTestowa";

                SqlCommand sqlCommand = new SqlCommand(query, sqlConnection);
                

                using(var sqlReader = sqlCommand.ExecuteReader())
                {
                    while (sqlReader.Read())
                    {
                        print(sqlReader[0].ToString());
                        print(sqlReader[1].ToString());

                    }
                }
            }
            #endregion

            #region POLACZENIE_PRZEZ_ODBC

            print("=================================================================");
            print("Czytanie danych z bazy danych przez ODBC");
            using (OdbcConnection odbcConnection = new OdbcConnection(odbcStringConnection))
            {
                try
                {
                    odbcConnection.Open();
                    string query = "SELECT * FROM dbo.TabelaTestowa";

                    OdbcCommand odbcCommand = new OdbcCommand(query, odbcConnection);

                    using (var sqlReader = odbcCommand.ExecuteReader())
                    {
                        while (sqlReader.Read())
                        {
                            print(sqlReader[0].ToString());
                            print(sqlReader[1].ToString());

                        }
                    }
                }
                catch (OdbcException e)
                {
                    print("Wystapil blad podczas polaczenia przez ODBC");
                }
                catch (Exception e)
                {
                    print("Wystapil niezidentyfikowany wyjatek." + e.Message);
                }

            }

            #endregion  
            // komentuje ze wzgledu na to ze to byl testowy insert do testowej tabeli
            #region ZAPIS_DO_BAZY_DANYCH_DB_FIRST
            /*
                        SzkolenieLSzopaContext context = new SzkolenieLSzopaContext();
                        TabelaTestowa tabelaTestowa = new TabelaTestowa();
                        tabelaTestowa.Nazwa = "Test z Visual";
                        context.TabelaTestowa.Add(tabelaTestowa);
                        context.SaveChanges();
                        */
            #endregion


            #region ZAPIS_DO_BAZY_DANYCH_CODE_FIRST
            DatabaseContext databaseContext = new DatabaseContext();
            databaseContext.Rodzina.Add(rodzina);
           /* var rodzinaDoUsuniecia = databaseContext.Rodzina.Include(p => p.ListaUzytkownikow).First(r => r.ID == 1);
            var uzytkownikDoUsuniecia = databaseContext.Uzytkownik.Where(p => 
                rodzinaDoUsuniecia.ListaUzytkownikow.Contains(p)).ToList();
            databaseContext.Uzytkownik.RemoveRange(uzytkownikDoUsuniecia);
            databaseContext.Remove(databaseContext.Rodzina.First(p => p.ID ==1));*/
            databaseContext.SaveChanges();
#endregion



            //===============================================================================================

#endif

        }

        static void DodajDaneInit()
        {
            print("Podaj imie i nazwisko:");
            //string[] imieNazwisko = Console.ReadLine().Split(" ");
            string[] imieNazwisko = "Jan Kowalski".Split(" ");

            print("Podaj pelny adres zamieszkania:");
            //string adres = Console.ReadLine();
            string adres = "ul. Kwiatowa 15\n35-166\nKedzierzyn-Kozle";

            Adres adresObjekt = new Adres(adres);
            print(adresObjekt.ToString());

            Uzytkownik user = new Rodzic(imieNazwisko[0], imieNazwisko[1], adresUzytkownika: adresObjekt);

            var listaIUzytkowniks = new List<Uzytkownik>();
            Rodzina rodzina = new Rodzina(listaIUzytkowniks);
            rodzina.ListaUzytkownikow.Add(user);
            rodzina.ListaUzytkownikow.Add(new Dziecko("Łukasz", "Szopa", 55, 164, 13));
            rodzina.ListaUzytkownikow.Add(new Dziecko("Joanna", "Nowak", 37, 110, 9));

            BinarySerializator.Serializuj(adresObjekt);
            BinarySerializator.Serializuj(rodzina);
        }



        static void DodajJeden(out int liczba)
        {
            liczba = 13;
            liczba++;
        }

        static void print(string stringToPrint)
        {
            Console.WriteLine(stringToPrint);
        }

        static IEnumerable<int> ZwrocParzyste(IEnumerable<int> kolekcja)
        {
            foreach (var elem in kolekcja)
            {
                if (elem % 2 == 0)
                {
                    yield return elem;
                }
            }
        }
    }
}
