﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Obiekty
{
    [Serializable]
    public class Dziecko: Uzytkownik
    {
        private int Waga { get; set; }
        private int Wzrost { get; set; }

        public Dziecko(string imie, string nazwisko, int waga, int wzrost, int wiek, Adres adresUzytkownika = null) : base(imie, nazwisko, wiek, adresUzytkownika)
        {
            Waga = waga;
            Wzrost = wzrost;
        }

        public int PrzewidywalnyWzrost(int wiekPrzy)
        {
            return Wzrost + ((wiekPrzy - Wiek) * 10);
        }

        public override string PrzedstawSie()
        {
            return "Nazywam sie " + Imie + " " + Nazwisko;
        }
    }
}
