﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Obiekty.Migrations
{
    public partial class Migracja1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Adres",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Miejscowosc = table.Column<string>(nullable: true),
                    KodPocztowyString = table.Column<string>(nullable: true),
                    Prefiks = table.Column<string>(nullable: true),
                    Ulica = table.Column<string>(nullable: true),
                    NumerDomu = table.Column<string>(nullable: true),
                    NumerMieszkania = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Adres", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Rodzina",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Rodzina", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Uzytkownik",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Imie = table.Column<string>(nullable: true),
                    Nazwisko = table.Column<string>(nullable: true),
                    Wiek = table.Column<int>(nullable: false),
                    AdresID = table.Column<int>(nullable: true),
                    RodzinaID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Uzytkownik", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Uzytkownik_Adres_AdresID",
                        column: x => x.AdresID,
                        principalTable: "Adres",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Uzytkownik_Rodzina_RodzinaID",
                        column: x => x.RodzinaID,
                        principalTable: "Rodzina",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Uzytkownik_AdresID",
                table: "Uzytkownik",
                column: "AdresID");

            migrationBuilder.CreateIndex(
                name: "IX_Uzytkownik_RodzinaID",
                table: "Uzytkownik",
                column: "RodzinaID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Uzytkownik");

            migrationBuilder.DropTable(
                name: "Adres");

            migrationBuilder.DropTable(
                name: "Rodzina");
        }
    }
}
