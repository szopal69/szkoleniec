﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Obiekty
{
    [Serializable]
    public class Adres
    {
        public string Miejscowosc { get; set; }

//        [NonSerialized]
  //      string _kodPocztowy;

        [NotMapped]
        internal KodPocztowy KodPocztowy { get; set; }
        public string KodPocztowyString { get; set; }
        public string Prefiks { get; set; }
        public string Ulica { get; set; }
        public string NumerDomu { get; set; }
        public string NumerMieszkania { get; set; }

        public int ID { get; set; }
        public Adres()
        {

        }
        /// <summary>
        /// Konstruktor Adresu
        /// </summary>
        /// <param name="adres">Adres w formacie: :prefiks. :ulica :numer_domu :kod_pocztowy :miejscowosc</param>
        public Adres(string adres)
        {
            string[] tablicaDanychAdresowych = adres.Split("\n ".ToCharArray());

            Prefiks = tablicaDanychAdresowych[0];
            Ulica = tablicaDanychAdresowych[1];
            NumerDomu = tablicaDanychAdresowych[2];
            //KodPocztowy = new KodPocztowy(tablicaDanychAdresowych[5]);
            KodPocztowy = tablicaDanychAdresowych[3];
            Miejscowosc = tablicaDanychAdresowych[4];
            //sdfjsdijofj
        }

        public override string ToString()
        {
            return $"Prefiks: {Prefiks}\nUlica: {Ulica}\nNumer domu:{NumerDomu}\nKod pocztowy:{KodPocztowy}\nMiejscowosc: {Miejscowosc}\nNazwa regionu: {KodPocztowy.nazwaRegionu}\nNazwa miasta: {KodPocztowy.nazwaMiasta}";
        }
        
    }
}
