﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Obiekty
{
    [Serializable]
    public struct KodPocztowy
    {
        public enum RegionEnum { Warszawski=0, Olsztynski=1, Lubelski=2, Krakowski=3,
            Katowicki=4, Wroclawski=5, Poznanski=6, Szczecinski=7, Gdanski=8, Lodzki=9}

        internal enum MiastoEnum { Warszawa1=0, Warszawa2=1, Rzeszow=35, Gdansk=80, Niezdefiniowany=-1}

        internal RegionEnum nazwaRegionu { get; }

        internal MiastoEnum nazwaMiasta { get; }

        internal char[] kodPocztowyChar;

        internal KodPocztowy(string kodPocztowy)
        {
            string messageException = "Kod pocztowy jest niepoprawny";

            nazwaRegionu = (RegionEnum)Int32.Parse(kodPocztowy[0].ToString());

            int numerMiasta;

            if(int.TryParse(kodPocztowy.Substring(0, 2), out numerMiasta))
            {
                if(Enum.IsDefined(typeof(MiastoEnum), numerMiasta))
                {
                    nazwaMiasta = (MiastoEnum) numerMiasta;
                }
                else
                {
                    nazwaMiasta = MiastoEnum.Niezdefiniowany;
                }
            }
            else
            {
                nazwaMiasta = MiastoEnum.Niezdefiniowany;
            }


            if (kodPocztowy.Contains("-") && kodPocztowy.IndexOf("-") == 2 && kodPocztowy.Length == 6)
            {
                string kodPocztowyBezMyslnika = kodPocztowy.Remove(2, 1);
                int kodPocztowyBezMyslnikaInt;
                bool czyCyfry = int.TryParse(kodPocztowyBezMyslnika, out kodPocztowyBezMyslnikaInt);
                if (czyCyfry)
                {
                    kodPocztowyChar = kodPocztowyBezMyslnika.ToCharArray();
                } 
                else
                {
                    throw new ArgumentException(messageException);
                }
            }
            else
            {
                throw new ArgumentException(messageException);
            }

        }

        public static implicit operator KodPocztowy(string kodPocztowy)
        {
            return new KodPocztowy(kodPocztowy);
        }

        public static implicit operator string(KodPocztowy kodPocztowy)
        {
            return kodPocztowy;
        }

        public override string ToString()
        {
            
            return $"{kodPocztowyChar[0]}{kodPocztowyChar[1]}-{kodPocztowyChar[2]}{kodPocztowyChar[3]}{kodPocztowyChar[4]}";
        }
    }

    
}
