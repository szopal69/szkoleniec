﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Obiekty
{
    [Serializable]
    public class Rodzic:Uzytkownik
    {
        public Rodzic(string imie, string nazwisko, int wiek = default, Adres adresUzytkownika = null) : base(imie, nazwisko, wiek, adresUzytkownika)
        { 
        }
    }
}
