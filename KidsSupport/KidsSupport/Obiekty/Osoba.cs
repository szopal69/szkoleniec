﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Obiekty
{
    [Serializable]
    public abstract class Osoba
    {
        public string Imie { get; set; }

        [NonSerialized]
        string _nazwisko;
        public string Nazwisko
        {
            get
            {
                return _nazwisko;
            }

            set
            {
                _nazwisko = value.Substring(0, 1).ToUpper() + value.Substring(1).ToLower();
            }
        }
        public int Wiek { get; set; }
        public Adres Adres { get; set; }

        public Osoba()
        {

        }
        public Osoba(string imie, string nazwisko, int wiek=default, Adres adresOsoby =null)
        {
            Imie = imie;
            Nazwisko = nazwisko;
            Wiek = wiek;
            
            Adres = adresOsoby;
        }

        public override string ToString()
        {
            return $"Imie: {Imie}\nNazwisko: {Nazwisko}\n";
        }

        public virtual string PrzedstawSie()
        {
            return "Nazywam sie Pan " + Imie + " " + Nazwisko;
        }
    }
}
