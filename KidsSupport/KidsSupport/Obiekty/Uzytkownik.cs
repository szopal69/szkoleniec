﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualBasic.CompilerServices;

namespace Obiekty
{
    [Serializable]
    public class Uzytkownik : Osoba, IUzytkownik
    {
        public int ID { get; set; }
        


        public Uzytkownik()
        {

        }
        public Uzytkownik(string imie, string nazwisko, int wiek=default, Adres adresUzytkownika = null) : base( imie, nazwisko, wiek, adresUzytkownika)
        {

        }

        public static Rodzina operator+(Uzytkownik uzytkownik1, Uzytkownik uzytkownik2)
        {
            List<Uzytkownik> listaUzytkownikow = new List<Uzytkownik>();
            listaUzytkownikow.Add(uzytkownik1);
            listaUzytkownikow.Add(uzytkownik2);
            Rodzina rodzina = new Rodzina(listaUzytkownikow);
            return rodzina;
        }
        
            
    }
}
