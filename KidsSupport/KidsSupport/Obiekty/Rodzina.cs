﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obiekty
{
    [Serializable]
    public class Rodzina
    {
        public IList<Uzytkownik> ListaUzytkownikow { get; set; }

        public int ID { get; set; }
        public Rodzina()
        {

        }

        public Rodzina(IList<Uzytkownik> listaUzytkownikow)
        {
            ListaUzytkownikow = listaUzytkownikow;
        }

        public int IleOkregow()
        {
            return ListaUzytkownikow.Select(p => p.Adres?.KodPocztowy.nazwaRegionu).Distinct().Count(p => p != null);
        }

        public static Rodzina operator+(Rodzina rodzina, Uzytkownik uzytkownik)
        {
            rodzina.ListaUzytkownikow.Add(uzytkownik);
            return rodzina;
        }
    }
}
