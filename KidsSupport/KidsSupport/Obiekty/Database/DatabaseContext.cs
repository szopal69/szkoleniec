﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;

namespace Obiekty.Database
{
    public class DatabaseContext : DbContext
    {
        public DbSet<Rodzina> Rodzina { get; set; }
        public DbSet<Uzytkownik> Uzytkownik { get; set; }

        public DbSet<Adres> Adres { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder builder)
        {
            builder.UseSqlServer(
                @"Server=APS00079033-MB\SQLEXPRESS;Database=SzkolenieLSzopa;Integrated Security=SSPI;");

        }
    }
}
