﻿using System;
using System.Collections.Generic;
using System.Text;
using Obiekty;
using Xunit;

namespace Testy
{
    public class DzieckoTest
    {
        [Fact]
        public void PrzewidywalnyWzrostTest()
        {
            Dziecko dziecko = new Dziecko("Ala", "Nowak", 55, 156, 13);
            int result = dziecko.PrzewidywalnyWzrost(15);
            Assert.Equal(176, result);
        }
    }
}
