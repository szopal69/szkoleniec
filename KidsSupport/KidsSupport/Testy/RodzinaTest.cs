﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using Moq;
using Obiekty;
using Xunit;

namespace Testy
{

    public class RodzinaTest
    {
        [Fact]
        public void IleOkregowBezUzytkownikowTest()
        {
            var uzytkownicy = new List<Uzytkownik>();
            //uzytkownicy.Add(new IUzytkownik("Jan", "Kowalski", "43", new Adres("ul. Rzeszowska 33 35-084 Rzeszow")));
            Rodzina rodzina = new Rodzina(uzytkownicy);

            var licznik = rodzina.IleOkregow();
            Assert.Equal(0, licznik);
        }


        [Fact]
        public void IleOkregowDlaUzytkownikowTest()
        {
            //=================================================================================================================
            //===================   Ponizszy sposob definiowania obiektow jest prawidlowy natomiast Mock nie wspiera SETUP
            //var uzytkownicy = new Mock<List<IUzytkownik>>();
            //uzytkownicy.Setup(p => p.Select(It.IsAny<Func<IUzytkownik, KodPocztowy.RegionEnum>>())).Returns(3)
            //var rodzic = new Mock<Rodzic>();
            //var dziecko = new Mock<Dziecko>();
            //Rodzina rodzina = new Rodzina(uzytkownicy.Object);
            //================================================================================================================


            var uzytkownicy = new List<Uzytkownik>();
            uzytkownicy.Add(new Dziecko("Jas", "Kowalski", 23, 140, 12, new Adres("ul. Rzeszowska 33 35-084 Rzeszow")));
            uzytkownicy.Add(new Rodzic("Jas", "Kowalski", 23, new Adres("ul. Rzeszowska 33 35-084 Rzeszow")));

            uzytkownicy.Add(new Rodzic("Jas", "Kowalski", 23));

            Rodzina rodzina = new Rodzina(uzytkownicy);

            var licznik = rodzina.IleOkregow();
            Assert.Equal(1, licznik);
        }
    }
}
