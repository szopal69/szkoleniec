﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Api.Controllers;
using Api.Services;
using Moq;
using Obiekty;
using Xunit;

namespace Testy
{
    public class RodzinaControllerTest
    {
        [Fact]
        public void GetRodzinyTest()
        {
            var rodzinaSerwisMock = new Mock<IRodzinaSerwis>();
            var rodzinyMock = new Mock<IEnumerable<Rodzina>>();

            rodzinaSerwisMock.Setup(p => p.GetRodziny()).Returns(Task.FromResult(rodzinyMock.Object));

            var rodzinaController = new RodzinaController(rodzinaSerwisMock.Object);
            var result = rodzinaController.GetRodziny();
            Assert.Same(rodzinyMock.Object, result.Result.Value);

            rodzinaSerwisMock.Verify(p => p.GetRodziny(), Times.Once);
            rodzinaSerwisMock.VerifyNoOtherCalls();
            rodzinyMock.VerifyNoOtherCalls();
            
        }
    }
}
