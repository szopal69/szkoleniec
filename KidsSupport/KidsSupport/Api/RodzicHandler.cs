﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace Api
{
    public class RodzicHandler:AuthorizationHandler<RodzicRequirement>
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, RodzicRequirement requirement)
        {
            //context.Succeed(requirement);

            
            var email = context.User.Claims.FirstOrDefault(p =>
                p.Type.Equals("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress"));
            if (email.Equals("szopal69@gmail.com"))
            {
                context.Succeed(requirement);
            }
           // context.Succeed(requirement);
            return Task.CompletedTask;
        }
    }
}
