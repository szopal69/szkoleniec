﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Obiekty;

namespace Api.Services
{
    public interface IRodzinaSerwis
    {
        Task<IEnumerable<Rodzina>> GetRodziny();
    }
}
