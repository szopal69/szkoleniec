﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Obiekty;
using Obiekty.Database;

namespace Api.Services
{
    public class RodzinaSerwis : IRodzinaSerwis
    {
        private readonly DatabaseContext _databaseContext;

        public RodzinaSerwis(DatabaseContext databaseContext)
        {
            _databaseContext = databaseContext;
        }

        public async Task<IEnumerable<Rodzina>> GetRodziny()
        {
            return await _databaseContext.Rodzina.Include(p => p.ListaUzytkownikow).ToListAsync();
        }
    }
}
