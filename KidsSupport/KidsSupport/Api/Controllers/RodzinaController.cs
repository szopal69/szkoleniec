﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Api.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Obiekty;
using Obiekty.Database;

namespace Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Policy = "Rodzic")]
    public class RodzinaController : ControllerBase
    {
        private readonly IRodzinaSerwis rodzinaSerwis;

        public RodzinaController(IRodzinaSerwis rodzinaSerwis)
        {
            this.rodzinaSerwis = rodzinaSerwis;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Rodzina>>> GetRodziny()
        {
            var result = await rodzinaSerwis.GetRodziny();
            return new ActionResult<IEnumerable<Rodzina>>(result);
           
        }


    }
}