﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Obiekty;
using Obiekty.Database;

namespace Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RodzinyAutoController : ControllerBase
    {
        private readonly DatabaseContext _context;

        public RodzinyAutoController(DatabaseContext context)
        {
            _context = context;
        }

        // GET: api/RodzinyAuto
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Rodzina>>> GetRodzina()
        {
            return await _context.Rodzina.ToListAsync();
        }

        // GET: api/RodzinyAuto/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Rodzina>> GetRodzina(int id)
        {
            var rodzina = await _context.Rodzina.FindAsync(id);

            if (rodzina == null)
            {
                return NotFound();
            }

            return rodzina;
        }

        // PUT: api/RodzinyAuto/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutRodzina(int id, Rodzina rodzina)
        {
            if (id != rodzina.ID)
            {
                return BadRequest();
            }

            _context.Entry(rodzina).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RodzinaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/RodzinyAuto
        [HttpPost]
        public async Task<ActionResult<Rodzina>> PostRodzina(Rodzina rodzina)
        {
            _context.Rodzina.Add(rodzina);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetRodzina", new { id = rodzina.ID }, rodzina);
        }

        // DELETE: api/RodzinyAuto/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Rodzina>> DeleteRodzina(int id)
        {
            var rodzina = await _context.Rodzina.FindAsync(id);
            if (rodzina == null)
            {
                return NotFound();
            }

            _context.Rodzina.Remove(rodzina);
            await _context.SaveChangesAsync();

            return rodzina;
        }

        private bool RodzinaExists(int id)
        {
            return _context.Rodzina.Any(e => e.ID == id);
        }
    }
}
