﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Api.Services;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.Google;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Obiekty.Database;

namespace Api
{
    public class Startup
    {
        private static string clientId = "598278125050-uurqk146e1kljv9cielm8qh93v7sllnh.apps.googleusercontent.com";
        private static string clientSecretKey = "n7-rEhMJfxA3pm1qvR5SvX5o";
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<DatabaseContext>();


            
            services.AddAuthentication(p =>
            {
                p.DefaultAuthenticateScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                p.DefaultSignInScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                p.DefaultChallengeScheme = GoogleDefaults.AuthenticationScheme;
                p.DefaultSignOutScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                

            }).AddGoogle(p =>
                {
                    p.ClientId = clientId;
                    p.ClientSecret = clientSecretKey;
                    p.Scope.Add("profile");
                }).AddCookie();
                
            services.AddTransient<IRodzinaSerwis, RodzinaSerwis>();

            services.AddDbContext<DatabaseContext>();
            services.AddAuthorization(p =>
                p.AddPolicy("Rodzic", policy => policy.Requirements.Add(new RodzicRequirement())));
            services.AddSingleton<IAuthorizationHandler, RodzicHandler>();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseAuthentication();
            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
